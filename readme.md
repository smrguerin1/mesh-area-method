# Mesh Area Method

## Description

The mesh-area method algorithm was written by [Ségolène M. R. Guérin](mailto:segolene.guerin@mailbox.org) (last update on 25/08/2022).
This algorithm was designed to compute the fNIRS-headset variation in percentage from the beginning to the end of an experimental session. The input is one .tsv data file for each participant, with the Cartesian coordinates (i.e., x, y, and z position) for three markers (i.e., one the participant's temple and two on the fNIRS headset).

## License

Copyright (C) 2022 [Ségolène M. R. Guérin](mailto:segolene.guerin@mailbox.org)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Folder architecture

```
--- guerin_mesh_area_method_algorithm.Rmd

--- data
      |mocap  
	|*.tsv
```
